# Antora aggregate-collector pipeline extension
:version: 0.1.0-beta.1

`@djencks/antora-aggregate-collector`

This Antora pipeline extension includes content in a component-version that is not in the Antora file system layout.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/antora-aggregate-collector/-/blob/main/README.adoc.

## Installation

Available through npm as @djencks/antora-aggregate-collector.

The project git repository is https://gitlab.com/djencks/antora-aggregate-collector
