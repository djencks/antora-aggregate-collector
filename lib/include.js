'use strict'

const picomatch = require('picomatch')
const read = require('./read')

module.exports.register = function ({ config }) {
  const logger = this.getLogger('@djencks/antora-aggregate-collector')
  config.logLevel && (logger.level = config.logLevel)
  const trace = logger.isLevelEnabled('trace')
  const debug = logger.isLevelEnabled('debug')
  const cvMappings = processConfig(config)
  debug && logger.debug({ msg: 'processed mappings', cvMappings })

  this.on('contentAggregated', ({ contentAggregate }) => {
    debug && logger.debug(`content aggregate length ${contentAggregate.length}`)
    contentAggregate.forEach(({ name, version, files }) => {
      debug && logger.debug(`considering component-version ${version}@${name} with ${files.length} files`)
      let fileMap
      cvMappings.forEach(({ nameMatch, versionMatch, localMappings, mappings }) => {
        if (nameMatch(name) && versionMatch(version)) {
          if (!fileMap) {
            fileMap = files.reduce((accum, f) => {
              const origin = originKey(f.src.origin)
              let originMap = accum.get(origin)
              if (!originMap) {
                originMap = new Map()
                accum.set(origin, originMap)
              }
              originMap.set(f.src.path, f)
              return accum
            }, new Map()
            )
          }
          fileMap.forEach((originMap, originKey) => {
            const origin = originUnkey(originKey, logger)
            debug && logger.debug(`considering origin ${origin}`)
            let originMappings = mappings || []
            if (localMappings) {
              const mappingFile = originMap.get(localMappings)
              if (mappingFile) {
                const mappingObj = read(mappingFile, logger, { name, version, origin })
                if (mappingObj) {
                  originMappings = originMappings.concat(processMappings(Array.isArray(mappingObj)
                    ? mappingObj
                    : mappingObj.mappings))
                }
              }
            }
            originMappings.forEach(({
              module, family,
              urlMatch, startPathMatch, branchTagMatch,
              pathMatch,
              relativeMaps,
            }) => {
              if (urlMatch(origin.url) &&
                  startPathMatch(origin.startPath) &&
                  branchTagMatch(origin)) {
                originMap.forEach((f) => {
                  trace && logger.debug(`considering file ${f.src.path}`)
                  if (pathMatch(f.src.path)) {
                    mapRelative(f, relativeMaps)
                    f.path = `modules/${module}/${family}s/${f.path}`
                    debug && logger.debug({ file: f.src, component: name, version }, `mapped file to ${f.path}`)
                  } else {
                    trace && logger.debug({
                      msg: 'no match',
                      url: urlMatch(f.src.origin.url),
                      startPath: startPathMatch(f.src.origin.startPath),
                      branchTag: branchTagMatch(f.src.origin),
                      path: pathMatch(f.src.path),
                    })
                  }
                })
              }
            })
          })
        }
      })
    }
    )
  })
}

const GROUP_RX = /{([^}]+)}/g

function expr (format) {
  return (ctx) => format.replace(GROUP_RX, (_, name) => ctx[name])
}

function mapRelative (f, relativeMaps) {
  relativeMaps.forEach(({ match, format }) => {
    const m = match(f.path, true)
    if (m.isMatch) {
      const ctx = m.match.groups
      f.path = format(ctx)
    }
  })
}

function picomatcher ({ include, exclude }) {
  const options = exclude ? { ignore: exclude } : {}
  return picomatch(include, options)
}

const TRUE = () => true

function processMappings (mappings) {
  return mappings.map(({ module, family, path, origin, relativemap }) => {
    const { url, startPath, branch, tag } = (origin || {})
    return {
      module,
      family,
      pathMatch: (path && path.include) ? picomatcher(path) : TRUE,
      urlMatch: (url && url.include) ? picomatcher(url) : TRUE,
      startPathMatch: (startPath && startPath.include) ? picomatcher(startPath) : TRUE,
      branchTagMatch: branchTagMatcher(branch, tag),
      relativeMaps: relativemap ? relativemap.map(({ match, format }) => {
        return { match: picomatch(match), format: expr(format) }
      }) : [],
    }
  })
}

function branchTagMatcher (branch, tag) {
  const branchMatch = (branch && branch.include) ? picomatch(branch) : false
  const tagMatch = (tag && tag.include) ? picomatch(tag) : false
  if (branchMatch) {
    if (tagMatch) {
      return (origin) => branchMatch(origin.branch) || tagMatch(origin.tag)
    }
    return (origin) => branchMatch(origin.branch)
  } else if (tagMatch) {
    return (origin) => tagMatch(origin.tag)
  }
  return TRUE
}

function processConfig (config) {
  return config.componentversions.map(({ name, version, localMappings, mappings }) => {
    return {
      nameMatch: name ? picomatch(name) : TRUE,
      versionMatch: version ? picomatch(version) : TRUE,
      localMappings,
      mappings: mappings && processMappings(mappings),
    }
  })
}

// our own encoding is faster, but insignificantly.  For 100,000 calls, 4 ms vs. 28 ms
// ours doesn't work...
function originKey (origin) {
  return JSON.stringify(origin)
  // return `${origin.url}\n${origin.startPath}\n${origin.branch}\n${origin.tag}`
}

// const UNKEY_RX = /^(?<url>[^\n]*)\n(?<startPath>[^\n]*)\n(?<branch>[^\n]*)\n(?<tag>[^\n]*)$/

function originUnkey (key, logger) {
  return JSON.parse(key)
  // const m = key.match(UNKEY_RX)
  // if (m) return m.groups
  // logger.warn(`Programming error, unable to decode key ${key}`)
  // return {}
}

module.exports.expr = expr
module.exports.mapRelative = mapRelative
module.exports.processConfig = processConfig
module.exports.originKey = originKey
module.exports.originUnkey = originUnkey
