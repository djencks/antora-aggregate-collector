'use strict'

const json = require('json5')
const toml = require('@iarna/toml')
const yaml = require('js-yaml')

const parsers = {
  '.json': json.parse,
  '.toml': toml.parse,
  '.yaml': yaml.load,
  '.yml': yaml.load,
}

module.exports = (target, logger, useLogContext) => {
  const ext = target.src.extname
  const parse = parsers[ext]
  if (parse) {
    const contents = target.contents
    if (contents) {
      try {
        return parse(contents.toString())
      } catch (e) {
        logger.warn({ msg: `Invalid source file ${target}, error: ${e.msg}`, ...useLogContext })
      }
    } else {
      logger.warn({ msg: `Target ${target} not found`, ...useLogContext })
    }
  } else {
    logger.warn({ msg: `No parser for extension ${ext} for target {target}`, ...useLogContext })
  }
}
